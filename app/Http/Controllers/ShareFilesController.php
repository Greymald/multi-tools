<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ShareFilesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $files = Storage::allFiles('public/filesToShare');
        $files = array_map(function($elem){return str_ireplace('public/filesToShare/', '', $elem);}, $files);

        return view('shareFiles', ['files' => $files]);
    }

    public function download(Request $request)
    {
            return Storage::download('public/filesToShare/'.$request->name);
    }
}
