<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class PermissionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $users = User::all();
        return view('permission', ['users' => $users]);
    }

    public function update(Request $request)
    {
        $user = User::find($request->id);
        $datas = $this->extractPermission($request->all());

        foreach ($datas as $key => $data)
            $user->$key = $datas[$key];

        $user->save();
        return response()->json($user, 200);
    }

    public function create(Request $request)
    {
        $user = new User();
        $datas = $this->extractPermission($request->all());

        foreach ($datas as $key => $data)
            $user->$key = $datas[$key];

        $user->save();
        return response()->json($user, 201);
    }

    public function delete(User $user)
    {
        $user->delete();
        return response()->json($user, 200);
    }

    private function extractPermission(array $datas)
    {
        $formatedData = [];
        $permissions = [];
        foreach ($datas as $key => $data)
        {
            if (substr($key, 0, 10) === 'permission')
                $permissions[str_replace('permission_', '', $key)] = $data == "true";
            else if($key == 'password' && $data !== null)
                $formatedData[$key] = Hash::make($data);
            else if($data !== null)
                $formatedData[$key] = $data;
        }
        $formatedData['permissions'] = json_encode($permissions);
        return $formatedData;
    }
}
