<?php

namespace App\Http\Controllers;

use App\Models\Ingredient;
use App\Models\Recipe;
use Exception;
use App\Repository\IngredientRepositoryInterface;
use App\Repository\RecipeRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

class RecipeIngredientController extends Controller
{
    private $ingredientRepository;
    private $recipeRepository;

    public function __construct(IngredientRepositoryInterface $ingredientRepository, RecipeRepositoryInterface $recipeRepository)
    {
        $this->ingredientRepository = $ingredientRepository;
        $this->recipeRepository = $recipeRepository;
    }

    public function withRecipe(Recipe $recipe)
    {
        return $this->ingredientRepository->byRecipe($recipe);
    }

    public function exclude(Recipe $recipe)
    {
        return $this->ingredientRepository->exclude($recipe);
    }

    public function link(Request $request)
    {
        try {
            $recipe = Recipe::find($request->input('recipe'));
            $ingredient = Ingredient::find($request->input('ingredient'));

            $this->recipeRepository->linkIngredient($recipe, $ingredient);
            return response()->json($recipe, 200);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        }
    }

    public function unlink(Ingredient $ingredient, Recipe $recipe)
    {
        try {
            $this->recipeRepository->unlinkIngredient($recipe, $ingredient);
            return response()->json(null, 204);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        }
    }
}
