<?php

namespace App\Http\Controllers\QuickAuth;

use App\Http\Controllers\Controller;
use App\Models\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $password = $request->get('password');
        $userName = $request->get('name');
        $user = User::where('name', $userName)->first();
        $user->generateToken();

        if ($user && Hash::check($password, $user->password)){
            $request->session()->put('user', $user);
            $request->session()->put('connected', 'true');
            $request->session()->put('permission', $user->permissions);
            $request->session()->put('userName', $user->name);
            $request->session()->put('userId', $user->id);
            View()->share('guest', $request->session()->get('connected') !== 'true');
            View()->share('permission', $request->session()->get('permission'));
            View()->share('userName', $request->session()->get('userName'));
            View()->share('userId', $request->session()->get('userId'));
        }
        return redirect()->route('home');
    }

    public function check(Request $request)
    {
        $data = $request->all();
        $name = $data['name'];
        $password = $data['password'];
        $user = User::where('name', $name)->first();
        if ($user && Hash::check($password, $user->password))
            return response()->json(true);
        return response()->json(false);
    }

    protected function registered(Request $request, $user)
    {
        $user->generateToken();

        return response()->json(['data' => $user->toArray()], 201);
    }

}
