<?php

namespace App\Http\Controllers\QuickAuth;

use App\Http\Controllers\Controller;
use App\Models\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class LogoutController extends Controller
{
    public function logout(Request $request)
    {
        $user = User::find($request->session()->get('userId'));
        if ($user) {
            $user->api_token = null;
            $user->save();
            $request->session()->forget('connected');
        }

        return redirect()->route('home');
    }
}
