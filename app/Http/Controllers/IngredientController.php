<?php

namespace App\Http\Controllers;

use App\Repository\Eloquent\IngredientRepository;
use App\Repository\IngredientRepositoryInterface;
use App\Repository\RecipeRepositoryInterface;
use Exception;
use Illuminate\Support\Facades;
use App\Models\Ingredient;
use App\Models\Recipe;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

class IngredientController extends Controller
{
    private $ingredientRepository;
    private $recipeRepository;

    public function __construct(IngredientRepositoryInterface $ingredientRepository, RecipeRepositoryInterface $recipeRepository)
    {
        $this->ingredientRepository = $ingredientRepository;
        $this->recipeRepository = $recipeRepository;
    }

    public function index(Recipe $recipe)
    {
        /*if ($recipe->id) {
            return $this->recipeRepository->getIngredients($recipe);
        }*/
        return $this->ingredientRepository->all();
    }

    public function show(Ingredient $ingredient)
    {
        return $ingredient;
    }

    public function store(Request $request)
    {
        try{
            $ingredient = $this->ingredientRepository->store($request);
            return response()->json($ingredient, 201);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        }
    }

    public function delete(Ingredient $ingredient)
    {
        try {
            $ingredient->delete();
            return response()->json(null, 204);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        }
    }
}
