<?php

namespace App\Http\Controllers;

use App\Repository\IngredientRepositoryInterface;
use App\Repository\RecipeRepositoryInterface;
use Illuminate\Http\Request;
use Exception;
use App\Models\Recipe;

class RecipeController extends Controller
{
    private $ingredientRepository;
    private $recipeRepository;

    public function __construct(IngredientRepositoryInterface $ingredientRepository, RecipeRepositoryInterface $recipeRepository)
    {
        $this->ingredientRepository = $ingredientRepository;
        $this->recipeRepository = $recipeRepository;
    }

    public function index()
    {
        $recipes = $this->recipeRepository->all();

        //we determine if it's a vegetarian recipe
        foreach ($recipes as $k => $recipe){
            foreach ($recipe->ingredients()->get() as $ingredient)
            {
                if($ingredient->meat)
                    $recipe->setAttribute('meat', true);
            }
        }

        return $recipes;
    }

    public function show(Recipe $recipe)
    {
        return $recipe;
    }

    public function store(Request $request)
    {
        try {
            $recipe = $this->recipeRepository->store($request);
            return response()->json($recipe, 201);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        }
    }

    public function update(Request $request, Recipe $recipe)
    {
        try {
            $recipe = $this->recipeRepository->update($recipe, $request);
            return response()->json($recipe, 200);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        }
    }

    public function delete(Recipe $recipe)
    {
        try {
                $this->recipeRepository->delete($recipe);
                return response()->json([], 204);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        }
    }

    public function getRandom(Request $request)
    {
        // get parameters
        $number = $request->nb;
        $vegeNbr = $request->nbVege;
        $meatNbr = $number - $vegeNbr;

        // get random recipes
        $vegeRecipe = $this->recipeRepository->getVege();
        $vegeRecipe = count($vegeRecipe) < $vegeNbr ? $vegeRecipe : $vegeRecipe->random($vegeNbr);

        $meatRecipe = $this->recipeRepository->getMeat();
        $meatRecipe = count($meatRecipe) < $meatNbr ? $meatRecipe : $meatRecipe->random($meatNbr);

        $recipeList = $vegeRecipe->merge($meatRecipe);

        // extract ingredient list
        $ingredientList = [];
        foreach ($recipeList as $recipe){
            foreach ($recipe->ingredients as $ingredient){
                if (isset($ingredientList[$ingredient->name]))
                    $ingredientList[$ingredient->name]++;
                else
                    $ingredientList[$ingredient->name] = 1;
            }
        }

        return response()->json(['recipeList' => $recipeList, 'ingredientList' => $ingredientList], 200);
    }
}
