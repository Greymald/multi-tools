<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\View\View;

class checkConnection
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */


    public function handle($request, Closure $next)
    {
        $allowed = $this->isAllowed($request->session()->get('permission'), $request->route()->action['as']);
        $user = $request->session()->get('user');

        if ($user) {
            $user->refreshTokenExpiration();

            // share information to the views
            View()->share('guest', $request->session()->get('connected') !== 'true');
            View()->share('permission', $request->session()->get('permission'));
            View()->share('userName', $request->session()->get('userName'));
            View()->share('userId', $request->session()->get('userId'));
            View()->share('user', $user);
        }

        if (($request->session()->get('connected') !== 'true' || !$allowed) && $request->route()->uri !== "/")
            return redirect()->route('home');

        return $next($request);
    }

    private function isAllowed($permission, $fullNameRoute)
    {
        $namespace = explode('.', $fullNameRoute)[0];
        if ((isset($permission->$namespace) && $permission->$namespace === true))
            return true;
        else
            return false;
    }

}
