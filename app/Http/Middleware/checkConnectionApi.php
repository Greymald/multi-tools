<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\User;

class checkConnectionApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */


    public function handle(Request $request, Closure $next)
    {
        $user = User::where('api_token', $request->token)->first();
        $tokenExpiration = date($user->expiration_time_token);

        if($user && $tokenExpiration > now()){
            return $next($request);
        }
        else
            return response()->view('error', ['error' => '405'], 405);

    }

    private function isAllowed($permission, $fullNameRoute)
    {
        $namespace = explode('.', $fullNameRoute)[0];
        if ((isset($permission->$namespace) && $permission->$namespace === true))
            return true;
        else
            return false;
    }

}
