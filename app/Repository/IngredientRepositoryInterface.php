<?php
namespace App\Repository;

use App\Models\Ingredient;
use Illuminate\Support\Collection;

interface IngredientRepositoryInterface
{
    public function all(): Collection;
}
