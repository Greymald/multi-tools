<?php
namespace App\Repository;

use App\Models\Recipe;
use Illuminate\Support\Collection;

interface RecipeRepositoryInterface
{
    public function all(): Collection;
}
