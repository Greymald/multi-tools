<?php

namespace App\Repository\Eloquent;

use App\Models\Ingredient;
use App\Models\Recipe;
use App\Repository\IngredientRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class IngredientRepository extends BaseRepository implements IngredientRepositoryInterface
{

    /**
     * IngredientRepository constructor.
     *
     * @param Ingredient $model
     */
    public function __construct(Ingredient $model)
    {
        parent::__construct($model);
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->model->all();
    }

    public function store(Request $request): Ingredient
    {
        return Ingredient::create($request->all());
    }

    public function delete(Ingredient $ingredient)
    {
        return $ingredient->destroy($ingredient->id);
    }

    public function byRecipe(Recipe $recipe)
    {
        return Ingredient::whereHas('recipes', function(Builder $query) use ($recipe) {
            $query->where('recipe_id', 'like', $recipe->id);
        })->get();
    }

    public function exclude(Recipe $recipe)
    {
        return Ingredient::whereDoesntHave('recipes', function(Builder $query) use ($recipe) {
            $query->where('recipe_id', 'like', $recipe->id);
        })->get();
    }
}
