<?php

namespace App\Repository\Eloquent;

use App\Models\Ingredient;
use App\Models\Recipe;
use App\Repository\RecipeRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class RecipeRepository extends BaseRepository implements RecipeRepositoryInterface
{

    /**
     * RecipeRepository constructor.
     *
     * @param Recipe $model
     */
    public function __construct(Recipe $model)
    {
        parent::__construct($model);
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->model->all();
    }

    public function store(Request $request): Recipe
    {
        return Recipe::create($request->all());
    }

    public function update(Recipe $recipe, Request $request)
    {
        return $recipe->update($request->all());
    }

    public function delete(Recipe $recipe)
    {
        return $recipe->destroy($recipe->id);
    }

    public function getIngredients(Recipe $model): Collection
    {
        return $model->ingredients()->get();
    }

    public function getVege()
    {
        return Recipe::with('ingredients')->whereDoesntHave('ingredients', function($query){
            $query->where('meat', true);
        })->get();
    }

    public function getMeat()
    {
        return Recipe::with('ingredients')->whereHas('ingredients', function($query){
            $query->where('meat', true);
        })->get();
    }

    public function linkIngredient(Recipe $recipe, Ingredient $ingredient)
    {
        $recipe->ingredients()->attach($ingredient);
    }

    public function unlinkIngredient(Recipe $recipe, Ingredient $ingredient)
    {
        $recipe->ingredients()->detach($ingredient->id);
    }
}
