<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    protected $table = 'recipes';

    protected $fillable = ['name', 'steps', 'user_id'];

    public function ingredients()
    {
        return $this->belongsToMany("App\Models\Ingredient", 'recipe_ingredient');
    }
}
