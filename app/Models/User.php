<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class User extends Model
{
    protected $table = 'users';

    protected $fillable = ['name', 'permissions', 'password'];

    public function getPermissionsAttribute($value)
    {
        return json_decode($value);
    }

    public function generateToken()
    {
        $this->api_token = Str::random(60);
        $this->expiration_time_token = date("Y-m-d H:i:s", strtotime ("+" . env('SESSION_LIFETIME', 'production') ." minute"));
        $this->save();

        return $this->api_token;
    }

    public function refreshTokenExpiration()
    {
        $this->expiration_time_token = date("Y-m-d H:i:s", strtotime ("+" . env('SESSION_LIFETIME', 'production') ." minute"));
        $this->save();
    }
}
