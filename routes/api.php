<?php

use Illuminate\Http\Request;

use App\Models\Ingredient;
use App\Models\Recipe;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('recipes/get-random', 'RecipeController@getRandom');
Route::post('/check', 'QuickAuth\LoginController@check')->name('check');

Route::middleware('checkConnectionApi')->group(function () {
    //API Ingredient
    Route::get('ingredients/', 'IngredientController@index');
    Route::get('ingredients/{ingredient}', 'IngredientController@show');
    Route::post('ingredients', 'IngredientController@store');
    Route::put('ingredients/{ingredient}', 'IngredientController@update');
    Route::delete('ingredients/{ingredient}', 'IngredientController@delete');

    //API Recipe_Ingredient
    Route::post('ingredients/recipe/link', 'RecipeIngredientController@link');
    Route::get('ingredients/recipe/{recipe}', 'RecipeIngredientController@withRecipe');
    Route::get('ingredients/exclude/{recipe}', 'RecipeIngredientController@exclude');
    Route::delete('ingredients/recipe/unlink/{ingredient}/{recipe}', 'RecipeIngredientController@unlink');

    //API Recipe
    Route::get('recipes', 'RecipeController@index');
        Route::get('recipes/{recipe}', 'RecipeController@show');
        Route::post('recipes', 'RecipeController@store');
        Route::put('recipes/{recipe}', 'RecipeController@update');
        Route::delete('recipes/{recipe}', 'RecipeController@delete');
});
