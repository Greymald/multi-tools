<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//auth routes
Route::get('/logout', 'QuickAuth\LogoutController@logout')->name('logout');

Route::post('/login', 'QuickAuth\LoginController@login')->name('login');

Route::middleware(['checkConnection'])->group(function () {
    Route::get('/', 'HomeController@index')->name('home');

    Route::name('shop.')->group(function(){
        Route::get('/shop', 'ShopController@index')->name('index');
        Route::get('/shopList', 'ShopController@shopListGenerator')->name('shopList');
    });

    Route::name('shareFiles.')->group(function(){
        Route::get('/shareFiles', 'ShareFilesController@index')->name('index');
        Route::get('/download', 'ShareFilesController@download')->name('download');
    });

    Route::name('permission.')->group(function(){
        Route::get('/permission', 'PermissionController@index')->name('index');
        Route::put('permission/{id}', 'PermissionController@update')->name('update');
        Route::post('permission', 'PermissionController@create')->name('create');
        Route::delete('permission/{user}', 'PermissionController@delete')->name('delete');
    });
});
