var modal = document.querySelector("#modal");
var closeButton = document.querySelector("#modal .close");
var form = document.querySelector(".modal-content form");

function toggleModal() {
    modal.classList.toggle("show-popup");
    modal.querySelector("#name").focus();
}

function windowOnClick(event) {
    if (event.target === modal)
        toggleModal();
}

function checkOnSubmit(event) {
    event.preventDefault();
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState === XMLHttpRequest.DONE) {
            if (xmlhttp.response !== 'true') {
                document.querySelector('#modal .error-text').innerHTML  = 'wrong!';
            } else {
                form.submit();
            }
        }
    };

    xmlhttp.open("POST", "api/check", true);
    xmlhttp.setRequestHeader("Content-Type", "application/json");
    xmlhttp.send(JSON.stringify({name: document.querySelector("#modal #name").value, password:document.querySelector("#modal #password").value}));
}

//bind events
closeButton.addEventListener("click", toggleModal);
window.addEventListener("click", windowOnClick);
form.addEventListener('submit', checkOnSubmit);
