var permission = document.querySelector("#permission-content");

$.ajaxSetup({

    headers: {

        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

    }

});

document.querySelectorAll('.btn-update').forEach(item => {
    item.addEventListener('click', event => {updateEvents(event)
    });
});

function updateEvents(event){
    var id = event.target.getAttribute('data-user-id');
    var url = event.target.getAttribute('data-url');

    var name = $("#user-"+id+" input[name=name]").val();
    var password = $("#user-"+id+" input[name=password]").val();
    var inputs = $("#user-"+id+" input");

    var data = {name:name, password:password};
    $.each(inputs, function(key, value){
        if (value.getAttribute('type') === "checkbox")
            data[value.getAttribute('name')] = value.checked;
    })

    $.ajax({
        type: "put",
        url: url,
        data:data,
        success:function(data){
            console.log(data);
        }
    }).done(function( data ) {
        console.log(data)
    });
}

document.querySelector('.btn-create').addEventListener('click', event => {
    var url = event.target.getAttribute('data-url');

    var name = $("#user-create input[name=name]").val();
    var password = $("#user-create input[name=password]").val();
    var inputs = $("#user-create input");
    var data = {name:name, password:password};
    $.each(inputs, function(key, value){
        if (value.getAttribute('type') === "checkbox")
            data[value.getAttribute('name')] = value.checked;
    });

    $.ajax({
        type: "post",
        url: url,
        data:data,
        success:function(data){
            resetCreateUserForm();
            appendNewUser(data);
        }
    }).done(function( data ) {
        console.log(data)
    });
});

document.querySelectorAll('.btn-delete').forEach(item => {
    item.addEventListener('click', event => {deleteEvents(event)});
});

function deleteEvents(event){
    var url = event.target.getAttribute('data-url');
    var id = event.target.getAttribute('data-user-id');

    $.ajax({
        type: "delete",
        url: url,
        success:function(data){
            console.log(data);
        }
    }).done(function( data ) {
        document.querySelector('#user-' + id).remove();
    });
}

function resetCreateUserForm(){
    var form = document.querySelector('#user-create');
    form.querySelector('input[name=password]').value = "";
    form.querySelector('input[name=name]').value = "";
    form.querySelectorAll("#user-create input").forEach(function(element){
        if (element.getAttribute('type') === "checkbox")
            element.checked = false;
    });
}

function appendNewUser(user){
    var newUserForm = document.querySelector('.user-row').cloneNode(true);
    var id = user.id;
    newUserForm.id = "user-" + id;
    newUserForm.querySelector('input[name=name]').value = user.name;
    newUserForm.querySelector('.btn-update').setAttribute('data-url', 'http://laravel-project.test/permission/' + id);
    newUserForm.querySelector('.btn-update').setAttribute('data-user-id', id);
    newUserForm.querySelector('.btn-delete').setAttribute('data-url', 'http://laravel-project.test/permission/' + id);
    newUserForm.querySelector('.btn-delete').setAttribute('data-user-id', id);

    var inputs = newUserForm.querySelectorAll("input");
    inputs.forEach(function(element){
        if (element.getAttribute('type') === "checkbox")
            element.checked = eval('user.permissions.' + element.getAttribute('name').replace('permission.', ''));
    });

    document.querySelector('#permission-content .card').insertBefore(newUserForm, document.querySelector('#user-create'));

    var existingUser = document.querySelectorAll('.existing-user');
    existingUser[existingUser.length - 1].querySelector('.btn-update').addEventListener('click', event => {updateEvents(event)});
    existingUser[existingUser.length - 1].querySelector('.btn-delete').addEventListener('click', event => {deleteEvents(event)});
}
