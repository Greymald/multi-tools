window.Vue = require('vue');
require('./modal');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('app', require('./components/homeComponents/App.vue').default);
Vue.component('presentation', require('./components/homeComponents/Presentation.vue').default);
Vue.component('home-menu', require('./components/homeComponents/HomeMenu.vue').default);
Vue.component('game-of-life', require('./components/homeComponents/GameOfLife.vue').default);
Vue.component('recursive-component', require('./components/homeComponents/RecursiveComponent.vue').default);
Vue.component('recursive-row', require('./components/homeComponents/RecursiveRow.vue').default);
Vue.component('triangle-pascal', require('./components/homeComponents/TrianglePascal.vue').default);
Vue.component('login-puzzle', require('./components/homeComponents/LoginPuzzle.vue').default);
Vue.component('shop-list', require('./components/homeComponents/ShopList.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
const app = new Vue({
    el: '#app-home',
    data: {
    }
});
