<div class="user-row existing-user" id="user-{{ $user->id }}">
    @csrf
    <div class="table-box">
        <input type="text" name="name" value="{{$user->name}}">
    </div>
    <div class="table-box">
        <input type="password" name="password" value="">
    </div>
    <div class="table-box">
        @foreach($user->permissions as $name => $permission)
            <label>{{$name}}</label><input type="checkbox" name="permission.{{$name}}" @if($permission === true) checked @endif>
        @endforeach
    </div>
    <div class="crud-action table-box">
        <span class="btn btn-update" data-url="{{ route('permission.update', ['id' => $user->id]) }}" data-user-id="{{ $user->id }}">update</span>
        <span class="btn btn-delete" data-url="{{ route('permission.delete', ['user' => $user->id]) }}" data-user-id="{{ $user->id }}">delete</span>
    </div>
</div>
