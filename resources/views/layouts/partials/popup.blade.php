<!-- The Modal -->
<div id="modal">

    <!-- Modal content -->
    <div class="modal-content container">
        <span class="close">&times;</span>
        <div>
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" required autocomplete="name">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">


                    </div>
                </div>


                <div class="form-group row">
                    <div class="col-md-4 text-md-right">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Login') }}
                        </button>
                    </div>
                    <div class="col-md-6 text-danger error-text"></div>
                </div>
            </form>
        </div>
    </div>

</div>
