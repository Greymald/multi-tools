<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        @component('layouts.nav', ['guest' => $guest])

        @endcomponent
        <main class="py-4">
            @yield('content')
        </main>
    </div>

    <!-- Scripts -->
    <!-- app.js contain the general js stuff we need, every other one contain specific stuff by pages -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    @if(Request::url() == route('shop.index'))
        <script src="{{ asset('js/shop.js') }}" defer></script>
    @elseif(Request::url() == route('shop.shopList'))
        <script src="{{ asset('js/shopList.js') }}" defer></script>
    @elseif(Request::url() == route('permission.index'))
        <script src="{{ asset('js/permission.js') }}" defer></script>
    @endif
</body>
</html>
