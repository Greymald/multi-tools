@php
    switch (Request::url()){
        case(route('shareFiles.index')) :
            $mainColor = 'bg-title-red';
            break;
        case(route('shop.index')) :
            $mainColor = 'bg-title-darkorange';
            break;
        case(route('permission.index')) :
            $mainColor = 'bg-title-dodgerblue';
            break;
        default:
            $mainColor = 'bg-white';
            break;
    }
@endphp

<nav class="navbar navbar-expand-md navbar-light {{$mainColor}}">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('app.name', 'Laravel') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">

            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @if($guest)
                    <li class="nav-item">
                        <span id="open-login">Login</span>
                    </li>
                @else
                    <li class="nav-item">
                        <a id="open-logout" href="{{ route('logout') }}">Logout</a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
<div id="menu">
    <ul class="container">
        @if(isset($permission->shareFiles) && $permission->shareFiles === true)
            @if(Request::url() !== route('shareFiles.index'))<a href="{{ route('shareFiles.index') }}"> @endif
                <li class="red-menu @if(Request::url() == route('shareFiles.index'))selected-menu @endif">file tools</li>
            @if(Request::url() !== route('shareFiles.index'))</a> @endif
        @endif

        @if(isset($permission->shop) && $permission->shop === true)
            @if(Request::url() !== route('shop.index'))<a href="{{ route('shop.index') }}"> @endif
                <li class="main-menu orange-menu @if(Request::url() == route('shop.index'))selected-menu @endif">shop tools</li>
            @if(Request::url() !== route('shop.index'))</a> @endif
        @endif

        @if(isset($permission->permission) && $permission->permission === true)
            @if(Request::url() !== route('permission.index'))<a href="{{ route('permission.index') }}"> @endif
                <li class="blue-menu @if(Request::url() == route('permission.index'))selected-menu @endif">permission manager</li>
            @if(Request::url() !== route('permission.index'))</a> @endif
        @endif
    </ul>
</div>
