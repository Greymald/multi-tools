@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Files</div>
                <div id="file-list">
                    @foreach($files as $file)
                        @component('layouts.partials.file', ['file' => $file])
                        @endcomponent
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
