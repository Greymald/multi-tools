@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div id="permission-content" class="col-md-12">
            <div class="card">
                <div id="column-name">
                    <span>name</span>
                    <span>password</span>
                    <span>permission</span>
                    <span>actions</span>
                </div>
                @each('layouts.partials.user', $users, 'user')
                <div class="user-row" id="user-create">
                    @csrf
                    <div class="table-box">
                        <input type="text" name="name" value="">
                    </div>
                    <div class="table-box">
                        <input type="password" name="password" value="">
                    </div>
                    <div class="table-box">
                            <label>shop</label><input type="checkbox" name="permission.shop">
                            <label>download</label><input type="checkbox" name="permission.download">
                            <label>permission</label><input type="checkbox" name="permission.permission">
                            <label>shareFiles</label><input type="checkbox" name="permission.shareFiles">
                    </div>
                    <div class="crud-action table-box">
                        <span class="btn btn-create" data-url="{{ route('permission.create') }}">create</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<script>

</script>
