@extends('layouts.app')

@section('content')
    <div class="container">
        <div id="shop-list-generator">
            <shop-list user-token="{{ $user->api_token }}"></shop-list>
        </div>
    </div>
@endsection

