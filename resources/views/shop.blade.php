@extends('layouts.app')

@section('content')
    <div class="container">
        <div id="shop">
            <shop user-id="{{ $user->id }}" user-token="{{ $user->api_token }}"></shop>
        </div>
    </div>
@endsection

