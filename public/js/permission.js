/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/permission.js":
/*!************************************!*\
  !*** ./resources/js/permission.js ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("var permission = document.querySelector(\"#permission-content\");\n$.ajaxSetup({\n  headers: {\n    'X-CSRF-TOKEN': $('meta[name=\"csrf-token\"]').attr('content')\n  }\n});\ndocument.querySelectorAll('.btn-update').forEach(function (item) {\n  item.addEventListener('click', function (event) {\n    updateEvents(event);\n  });\n});\n\nfunction updateEvents(event) {\n  var id = event.target.getAttribute('data-user-id');\n  var url = event.target.getAttribute('data-url');\n  var name = $(\"#user-\" + id + \" input[name=name]\").val();\n  var password = $(\"#user-\" + id + \" input[name=password]\").val();\n  var inputs = $(\"#user-\" + id + \" input\");\n  var data = {\n    name: name,\n    password: password\n  };\n  $.each(inputs, function (key, value) {\n    if (value.getAttribute('type') === \"checkbox\") data[value.getAttribute('name')] = value.checked;\n  });\n  $.ajax({\n    type: \"put\",\n    url: url,\n    data: data,\n    success: function success(data) {\n      console.log(data);\n    }\n  }).done(function (data) {\n    console.log(data);\n  });\n}\n\ndocument.querySelector('.btn-create').addEventListener('click', function (event) {\n  var url = event.target.getAttribute('data-url');\n  var name = $(\"#user-create input[name=name]\").val();\n  var password = $(\"#user-create input[name=password]\").val();\n  var inputs = $(\"#user-create input\");\n  var data = {\n    name: name,\n    password: password\n  };\n  $.each(inputs, function (key, value) {\n    if (value.getAttribute('type') === \"checkbox\") data[value.getAttribute('name')] = value.checked;\n  });\n  $.ajax({\n    type: \"post\",\n    url: url,\n    data: data,\n    success: function success(data) {\n      resetCreateUserForm();\n      appendNewUser(data);\n    }\n  }).done(function (data) {\n    console.log(data);\n  });\n});\ndocument.querySelectorAll('.btn-delete').forEach(function (item) {\n  item.addEventListener('click', function (event) {\n    deleteEvents(event);\n  });\n});\n\nfunction deleteEvents(event) {\n  var url = event.target.getAttribute('data-url');\n  var id = event.target.getAttribute('data-user-id');\n  $.ajax({\n    type: \"delete\",\n    url: url,\n    success: function success(data) {\n      console.log(data);\n    }\n  }).done(function (data) {\n    document.querySelector('#user-' + id).remove();\n  });\n}\n\nfunction resetCreateUserForm() {\n  var form = document.querySelector('#user-create');\n  form.querySelector('input[name=password]').value = \"\";\n  form.querySelector('input[name=name]').value = \"\";\n  form.querySelectorAll(\"#user-create input\").forEach(function (element) {\n    if (element.getAttribute('type') === \"checkbox\") element.checked = false;\n  });\n}\n\nfunction appendNewUser(user) {\n  var newUserForm = document.querySelector('.user-row').cloneNode(true);\n  var id = user.id;\n  newUserForm.id = \"user-\" + id;\n  newUserForm.querySelector('input[name=name]').value = user.name;\n  newUserForm.querySelector('.btn-update').setAttribute('data-url', 'http://laravel-project.test/permission/' + id);\n  newUserForm.querySelector('.btn-update').setAttribute('data-user-id', id);\n  newUserForm.querySelector('.btn-delete').setAttribute('data-url', 'http://laravel-project.test/permission/' + id);\n  newUserForm.querySelector('.btn-delete').setAttribute('data-user-id', id);\n  var inputs = newUserForm.querySelectorAll(\"input\");\n  inputs.forEach(function (element) {\n    if (element.getAttribute('type') === \"checkbox\") element.checked = eval('user.permissions.' + element.getAttribute('name').replace('permission.', ''));\n  });\n  document.querySelector('#permission-content .card').insertBefore(newUserForm, document.querySelector('#user-create'));\n  var existingUser = document.querySelectorAll('.existing-user');\n  existingUser[existingUser.length - 1].querySelector('.btn-update').addEventListener('click', function (event) {\n    updateEvents(event);\n  });\n  existingUser[existingUser.length - 1].querySelector('.btn-delete').addEventListener('click', function (event) {\n    deleteEvents(event);\n  });\n}//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvcGVybWlzc2lvbi5qcz9iZTdkIl0sIm5hbWVzIjpbInBlcm1pc3Npb24iLCJkb2N1bWVudCIsInF1ZXJ5U2VsZWN0b3IiLCIkIiwiYWpheFNldHVwIiwiaGVhZGVycyIsImF0dHIiLCJxdWVyeVNlbGVjdG9yQWxsIiwiZm9yRWFjaCIsIml0ZW0iLCJhZGRFdmVudExpc3RlbmVyIiwiZXZlbnQiLCJ1cGRhdGVFdmVudHMiLCJpZCIsInRhcmdldCIsImdldEF0dHJpYnV0ZSIsInVybCIsIm5hbWUiLCJ2YWwiLCJwYXNzd29yZCIsImlucHV0cyIsImRhdGEiLCJlYWNoIiwia2V5IiwidmFsdWUiLCJjaGVja2VkIiwiYWpheCIsInR5cGUiLCJzdWNjZXNzIiwiY29uc29sZSIsImxvZyIsImRvbmUiLCJyZXNldENyZWF0ZVVzZXJGb3JtIiwiYXBwZW5kTmV3VXNlciIsImRlbGV0ZUV2ZW50cyIsInJlbW92ZSIsImZvcm0iLCJlbGVtZW50IiwidXNlciIsIm5ld1VzZXJGb3JtIiwiY2xvbmVOb2RlIiwic2V0QXR0cmlidXRlIiwiZXZhbCIsInJlcGxhY2UiLCJpbnNlcnRCZWZvcmUiLCJleGlzdGluZ1VzZXIiLCJsZW5ndGgiXSwibWFwcGluZ3MiOiJBQUFBLElBQUlBLFVBQVUsR0FBR0MsUUFBUSxDQUFDQyxhQUFULENBQXVCLHFCQUF2QixDQUFqQjtBQUVBQyxDQUFDLENBQUNDLFNBQUYsQ0FBWTtBQUVSQyxTQUFPLEVBQUU7QUFFTCxvQkFBZ0JGLENBQUMsQ0FBQyx5QkFBRCxDQUFELENBQTZCRyxJQUE3QixDQUFrQyxTQUFsQztBQUZYO0FBRkQsQ0FBWjtBQVVBTCxRQUFRLENBQUNNLGdCQUFULENBQTBCLGFBQTFCLEVBQXlDQyxPQUF6QyxDQUFpRCxVQUFBQyxJQUFJLEVBQUk7QUFDckRBLE1BQUksQ0FBQ0MsZ0JBQUwsQ0FBc0IsT0FBdEIsRUFBK0IsVUFBQUMsS0FBSyxFQUFJO0FBQUNDLGdCQUFZLENBQUNELEtBQUQsQ0FBWjtBQUN4QyxHQUREO0FBRUgsQ0FIRDs7QUFLQSxTQUFTQyxZQUFULENBQXNCRCxLQUF0QixFQUE0QjtBQUN4QixNQUFJRSxFQUFFLEdBQUdGLEtBQUssQ0FBQ0csTUFBTixDQUFhQyxZQUFiLENBQTBCLGNBQTFCLENBQVQ7QUFDQSxNQUFJQyxHQUFHLEdBQUdMLEtBQUssQ0FBQ0csTUFBTixDQUFhQyxZQUFiLENBQTBCLFVBQTFCLENBQVY7QUFFQSxNQUFJRSxJQUFJLEdBQUdkLENBQUMsQ0FBQyxXQUFTVSxFQUFULEdBQVksbUJBQWIsQ0FBRCxDQUFtQ0ssR0FBbkMsRUFBWDtBQUNBLE1BQUlDLFFBQVEsR0FBR2hCLENBQUMsQ0FBQyxXQUFTVSxFQUFULEdBQVksdUJBQWIsQ0FBRCxDQUF1Q0ssR0FBdkMsRUFBZjtBQUNBLE1BQUlFLE1BQU0sR0FBR2pCLENBQUMsQ0FBQyxXQUFTVSxFQUFULEdBQVksUUFBYixDQUFkO0FBRUEsTUFBSVEsSUFBSSxHQUFHO0FBQUNKLFFBQUksRUFBQ0EsSUFBTjtBQUFZRSxZQUFRLEVBQUNBO0FBQXJCLEdBQVg7QUFDQWhCLEdBQUMsQ0FBQ21CLElBQUYsQ0FBT0YsTUFBUCxFQUFlLFVBQVNHLEdBQVQsRUFBY0MsS0FBZCxFQUFvQjtBQUMvQixRQUFJQSxLQUFLLENBQUNULFlBQU4sQ0FBbUIsTUFBbkIsTUFBK0IsVUFBbkMsRUFDSU0sSUFBSSxDQUFDRyxLQUFLLENBQUNULFlBQU4sQ0FBbUIsTUFBbkIsQ0FBRCxDQUFKLEdBQW1DUyxLQUFLLENBQUNDLE9BQXpDO0FBQ1AsR0FIRDtBQUtBdEIsR0FBQyxDQUFDdUIsSUFBRixDQUFPO0FBQ0hDLFFBQUksRUFBRSxLQURIO0FBRUhYLE9BQUcsRUFBRUEsR0FGRjtBQUdISyxRQUFJLEVBQUNBLElBSEY7QUFJSE8sV0FBTyxFQUFDLGlCQUFTUCxJQUFULEVBQWM7QUFDbEJRLGFBQU8sQ0FBQ0MsR0FBUixDQUFZVCxJQUFaO0FBQ0g7QUFORSxHQUFQLEVBT0dVLElBUEgsQ0FPUSxVQUFVVixJQUFWLEVBQWlCO0FBQ3JCUSxXQUFPLENBQUNDLEdBQVIsQ0FBWVQsSUFBWjtBQUNILEdBVEQ7QUFVSDs7QUFFRHBCLFFBQVEsQ0FBQ0MsYUFBVCxDQUF1QixhQUF2QixFQUFzQ1EsZ0JBQXRDLENBQXVELE9BQXZELEVBQWdFLFVBQUFDLEtBQUssRUFBSTtBQUNyRSxNQUFJSyxHQUFHLEdBQUdMLEtBQUssQ0FBQ0csTUFBTixDQUFhQyxZQUFiLENBQTBCLFVBQTFCLENBQVY7QUFFQSxNQUFJRSxJQUFJLEdBQUdkLENBQUMsQ0FBQywrQkFBRCxDQUFELENBQW1DZSxHQUFuQyxFQUFYO0FBQ0EsTUFBSUMsUUFBUSxHQUFHaEIsQ0FBQyxDQUFDLG1DQUFELENBQUQsQ0FBdUNlLEdBQXZDLEVBQWY7QUFDQSxNQUFJRSxNQUFNLEdBQUdqQixDQUFDLENBQUMsb0JBQUQsQ0FBZDtBQUNBLE1BQUlrQixJQUFJLEdBQUc7QUFBQ0osUUFBSSxFQUFDQSxJQUFOO0FBQVlFLFlBQVEsRUFBQ0E7QUFBckIsR0FBWDtBQUNBaEIsR0FBQyxDQUFDbUIsSUFBRixDQUFPRixNQUFQLEVBQWUsVUFBU0csR0FBVCxFQUFjQyxLQUFkLEVBQW9CO0FBQy9CLFFBQUlBLEtBQUssQ0FBQ1QsWUFBTixDQUFtQixNQUFuQixNQUErQixVQUFuQyxFQUNJTSxJQUFJLENBQUNHLEtBQUssQ0FBQ1QsWUFBTixDQUFtQixNQUFuQixDQUFELENBQUosR0FBbUNTLEtBQUssQ0FBQ0MsT0FBekM7QUFDUCxHQUhEO0FBS0F0QixHQUFDLENBQUN1QixJQUFGLENBQU87QUFDSEMsUUFBSSxFQUFFLE1BREg7QUFFSFgsT0FBRyxFQUFFQSxHQUZGO0FBR0hLLFFBQUksRUFBQ0EsSUFIRjtBQUlITyxXQUFPLEVBQUMsaUJBQVNQLElBQVQsRUFBYztBQUNsQlcseUJBQW1CO0FBQ25CQyxtQkFBYSxDQUFDWixJQUFELENBQWI7QUFDSDtBQVBFLEdBQVAsRUFRR1UsSUFSSCxDQVFRLFVBQVVWLElBQVYsRUFBaUI7QUFDckJRLFdBQU8sQ0FBQ0MsR0FBUixDQUFZVCxJQUFaO0FBQ0gsR0FWRDtBQVdILENBdkJEO0FBeUJBcEIsUUFBUSxDQUFDTSxnQkFBVCxDQUEwQixhQUExQixFQUF5Q0MsT0FBekMsQ0FBaUQsVUFBQUMsSUFBSSxFQUFJO0FBQ3JEQSxNQUFJLENBQUNDLGdCQUFMLENBQXNCLE9BQXRCLEVBQStCLFVBQUFDLEtBQUssRUFBSTtBQUFDdUIsZ0JBQVksQ0FBQ3ZCLEtBQUQsQ0FBWjtBQUFvQixHQUE3RDtBQUNILENBRkQ7O0FBSUEsU0FBU3VCLFlBQVQsQ0FBc0J2QixLQUF0QixFQUE0QjtBQUN4QixNQUFJSyxHQUFHLEdBQUdMLEtBQUssQ0FBQ0csTUFBTixDQUFhQyxZQUFiLENBQTBCLFVBQTFCLENBQVY7QUFDQSxNQUFJRixFQUFFLEdBQUdGLEtBQUssQ0FBQ0csTUFBTixDQUFhQyxZQUFiLENBQTBCLGNBQTFCLENBQVQ7QUFFQVosR0FBQyxDQUFDdUIsSUFBRixDQUFPO0FBQ0hDLFFBQUksRUFBRSxRQURIO0FBRUhYLE9BQUcsRUFBRUEsR0FGRjtBQUdIWSxXQUFPLEVBQUMsaUJBQVNQLElBQVQsRUFBYztBQUNsQlEsYUFBTyxDQUFDQyxHQUFSLENBQVlULElBQVo7QUFDSDtBQUxFLEdBQVAsRUFNR1UsSUFOSCxDQU1RLFVBQVVWLElBQVYsRUFBaUI7QUFDckJwQixZQUFRLENBQUNDLGFBQVQsQ0FBdUIsV0FBV1csRUFBbEMsRUFBc0NzQixNQUF0QztBQUNILEdBUkQ7QUFTSDs7QUFFRCxTQUFTSCxtQkFBVCxHQUE4QjtBQUMxQixNQUFJSSxJQUFJLEdBQUduQyxRQUFRLENBQUNDLGFBQVQsQ0FBdUIsY0FBdkIsQ0FBWDtBQUNBa0MsTUFBSSxDQUFDbEMsYUFBTCxDQUFtQixzQkFBbkIsRUFBMkNzQixLQUEzQyxHQUFtRCxFQUFuRDtBQUNBWSxNQUFJLENBQUNsQyxhQUFMLENBQW1CLGtCQUFuQixFQUF1Q3NCLEtBQXZDLEdBQStDLEVBQS9DO0FBQ0FZLE1BQUksQ0FBQzdCLGdCQUFMLENBQXNCLG9CQUF0QixFQUE0Q0MsT0FBNUMsQ0FBb0QsVUFBUzZCLE9BQVQsRUFBaUI7QUFDakUsUUFBSUEsT0FBTyxDQUFDdEIsWUFBUixDQUFxQixNQUFyQixNQUFpQyxVQUFyQyxFQUNJc0IsT0FBTyxDQUFDWixPQUFSLEdBQWtCLEtBQWxCO0FBQ1AsR0FIRDtBQUlIOztBQUVELFNBQVNRLGFBQVQsQ0FBdUJLLElBQXZCLEVBQTRCO0FBQ3hCLE1BQUlDLFdBQVcsR0FBR3RDLFFBQVEsQ0FBQ0MsYUFBVCxDQUF1QixXQUF2QixFQUFvQ3NDLFNBQXBDLENBQThDLElBQTlDLENBQWxCO0FBQ0EsTUFBSTNCLEVBQUUsR0FBR3lCLElBQUksQ0FBQ3pCLEVBQWQ7QUFDQTBCLGFBQVcsQ0FBQzFCLEVBQVosR0FBaUIsVUFBVUEsRUFBM0I7QUFDQTBCLGFBQVcsQ0FBQ3JDLGFBQVosQ0FBMEIsa0JBQTFCLEVBQThDc0IsS0FBOUMsR0FBc0RjLElBQUksQ0FBQ3JCLElBQTNEO0FBQ0FzQixhQUFXLENBQUNyQyxhQUFaLENBQTBCLGFBQTFCLEVBQXlDdUMsWUFBekMsQ0FBc0QsVUFBdEQsRUFBa0UsNENBQTRDNUIsRUFBOUc7QUFDQTBCLGFBQVcsQ0FBQ3JDLGFBQVosQ0FBMEIsYUFBMUIsRUFBeUN1QyxZQUF6QyxDQUFzRCxjQUF0RCxFQUFzRTVCLEVBQXRFO0FBQ0EwQixhQUFXLENBQUNyQyxhQUFaLENBQTBCLGFBQTFCLEVBQXlDdUMsWUFBekMsQ0FBc0QsVUFBdEQsRUFBa0UsNENBQTRDNUIsRUFBOUc7QUFDQTBCLGFBQVcsQ0FBQ3JDLGFBQVosQ0FBMEIsYUFBMUIsRUFBeUN1QyxZQUF6QyxDQUFzRCxjQUF0RCxFQUFzRTVCLEVBQXRFO0FBRUEsTUFBSU8sTUFBTSxHQUFHbUIsV0FBVyxDQUFDaEMsZ0JBQVosQ0FBNkIsT0FBN0IsQ0FBYjtBQUNBYSxRQUFNLENBQUNaLE9BQVAsQ0FBZSxVQUFTNkIsT0FBVCxFQUFpQjtBQUM1QixRQUFJQSxPQUFPLENBQUN0QixZQUFSLENBQXFCLE1BQXJCLE1BQWlDLFVBQXJDLEVBQ0lzQixPQUFPLENBQUNaLE9BQVIsR0FBa0JpQixJQUFJLENBQUMsc0JBQXNCTCxPQUFPLENBQUN0QixZQUFSLENBQXFCLE1BQXJCLEVBQTZCNEIsT0FBN0IsQ0FBcUMsYUFBckMsRUFBb0QsRUFBcEQsQ0FBdkIsQ0FBdEI7QUFDUCxHQUhEO0FBS0ExQyxVQUFRLENBQUNDLGFBQVQsQ0FBdUIsMkJBQXZCLEVBQW9EMEMsWUFBcEQsQ0FBaUVMLFdBQWpFLEVBQThFdEMsUUFBUSxDQUFDQyxhQUFULENBQXVCLGNBQXZCLENBQTlFO0FBRUEsTUFBSTJDLFlBQVksR0FBRzVDLFFBQVEsQ0FBQ00sZ0JBQVQsQ0FBMEIsZ0JBQTFCLENBQW5CO0FBQ0FzQyxjQUFZLENBQUNBLFlBQVksQ0FBQ0MsTUFBYixHQUFzQixDQUF2QixDQUFaLENBQXNDNUMsYUFBdEMsQ0FBb0QsYUFBcEQsRUFBbUVRLGdCQUFuRSxDQUFvRixPQUFwRixFQUE2RixVQUFBQyxLQUFLLEVBQUk7QUFBQ0MsZ0JBQVksQ0FBQ0QsS0FBRCxDQUFaO0FBQW9CLEdBQTNIO0FBQ0FrQyxjQUFZLENBQUNBLFlBQVksQ0FBQ0MsTUFBYixHQUFzQixDQUF2QixDQUFaLENBQXNDNUMsYUFBdEMsQ0FBb0QsYUFBcEQsRUFBbUVRLGdCQUFuRSxDQUFvRixPQUFwRixFQUE2RixVQUFBQyxLQUFLLEVBQUk7QUFBQ3VCLGdCQUFZLENBQUN2QixLQUFELENBQVo7QUFBb0IsR0FBM0g7QUFDSCIsImZpbGUiOiIuL3Jlc291cmNlcy9qcy9wZXJtaXNzaW9uLmpzLmpzIiwic291cmNlc0NvbnRlbnQiOlsidmFyIHBlcm1pc3Npb24gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3Blcm1pc3Npb24tY29udGVudFwiKTtcblxuJC5hamF4U2V0dXAoe1xuXG4gICAgaGVhZGVyczoge1xuXG4gICAgICAgICdYLUNTUkYtVE9LRU4nOiAkKCdtZXRhW25hbWU9XCJjc3JmLXRva2VuXCJdJykuYXR0cignY29udGVudCcpXG5cbiAgICB9XG5cbn0pO1xuXG5kb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcuYnRuLXVwZGF0ZScpLmZvckVhY2goaXRlbSA9PiB7XG4gICAgaXRlbS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGV2ZW50ID0+IHt1cGRhdGVFdmVudHMoZXZlbnQpXG4gICAgfSk7XG59KTtcblxuZnVuY3Rpb24gdXBkYXRlRXZlbnRzKGV2ZW50KXtcbiAgICB2YXIgaWQgPSBldmVudC50YXJnZXQuZ2V0QXR0cmlidXRlKCdkYXRhLXVzZXItaWQnKTtcbiAgICB2YXIgdXJsID0gZXZlbnQudGFyZ2V0LmdldEF0dHJpYnV0ZSgnZGF0YS11cmwnKTtcblxuICAgIHZhciBuYW1lID0gJChcIiN1c2VyLVwiK2lkK1wiIGlucHV0W25hbWU9bmFtZV1cIikudmFsKCk7XG4gICAgdmFyIHBhc3N3b3JkID0gJChcIiN1c2VyLVwiK2lkK1wiIGlucHV0W25hbWU9cGFzc3dvcmRdXCIpLnZhbCgpO1xuICAgIHZhciBpbnB1dHMgPSAkKFwiI3VzZXItXCIraWQrXCIgaW5wdXRcIik7XG5cbiAgICB2YXIgZGF0YSA9IHtuYW1lOm5hbWUsIHBhc3N3b3JkOnBhc3N3b3JkfTtcbiAgICAkLmVhY2goaW5wdXRzLCBmdW5jdGlvbihrZXksIHZhbHVlKXtcbiAgICAgICAgaWYgKHZhbHVlLmdldEF0dHJpYnV0ZSgndHlwZScpID09PSBcImNoZWNrYm94XCIpXG4gICAgICAgICAgICBkYXRhW3ZhbHVlLmdldEF0dHJpYnV0ZSgnbmFtZScpXSA9IHZhbHVlLmNoZWNrZWQ7XG4gICAgfSlcblxuICAgICQuYWpheCh7XG4gICAgICAgIHR5cGU6IFwicHV0XCIsXG4gICAgICAgIHVybDogdXJsLFxuICAgICAgICBkYXRhOmRhdGEsXG4gICAgICAgIHN1Y2Nlc3M6ZnVuY3Rpb24oZGF0YSl7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhKTtcbiAgICAgICAgfVxuICAgIH0pLmRvbmUoZnVuY3Rpb24oIGRhdGEgKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKGRhdGEpXG4gICAgfSk7XG59XG5cbmRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5idG4tY3JlYXRlJykuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBldmVudCA9PiB7XG4gICAgdmFyIHVybCA9IGV2ZW50LnRhcmdldC5nZXRBdHRyaWJ1dGUoJ2RhdGEtdXJsJyk7XG5cbiAgICB2YXIgbmFtZSA9ICQoXCIjdXNlci1jcmVhdGUgaW5wdXRbbmFtZT1uYW1lXVwiKS52YWwoKTtcbiAgICB2YXIgcGFzc3dvcmQgPSAkKFwiI3VzZXItY3JlYXRlIGlucHV0W25hbWU9cGFzc3dvcmRdXCIpLnZhbCgpO1xuICAgIHZhciBpbnB1dHMgPSAkKFwiI3VzZXItY3JlYXRlIGlucHV0XCIpO1xuICAgIHZhciBkYXRhID0ge25hbWU6bmFtZSwgcGFzc3dvcmQ6cGFzc3dvcmR9O1xuICAgICQuZWFjaChpbnB1dHMsIGZ1bmN0aW9uKGtleSwgdmFsdWUpe1xuICAgICAgICBpZiAodmFsdWUuZ2V0QXR0cmlidXRlKCd0eXBlJykgPT09IFwiY2hlY2tib3hcIilcbiAgICAgICAgICAgIGRhdGFbdmFsdWUuZ2V0QXR0cmlidXRlKCduYW1lJyldID0gdmFsdWUuY2hlY2tlZDtcbiAgICB9KTtcblxuICAgICQuYWpheCh7XG4gICAgICAgIHR5cGU6IFwicG9zdFwiLFxuICAgICAgICB1cmw6IHVybCxcbiAgICAgICAgZGF0YTpkYXRhLFxuICAgICAgICBzdWNjZXNzOmZ1bmN0aW9uKGRhdGEpe1xuICAgICAgICAgICAgcmVzZXRDcmVhdGVVc2VyRm9ybSgpO1xuICAgICAgICAgICAgYXBwZW5kTmV3VXNlcihkYXRhKTtcbiAgICAgICAgfVxuICAgIH0pLmRvbmUoZnVuY3Rpb24oIGRhdGEgKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKGRhdGEpXG4gICAgfSk7XG59KTtcblxuZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmJ0bi1kZWxldGUnKS5mb3JFYWNoKGl0ZW0gPT4ge1xuICAgIGl0ZW0uYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBldmVudCA9PiB7ZGVsZXRlRXZlbnRzKGV2ZW50KX0pO1xufSk7XG5cbmZ1bmN0aW9uIGRlbGV0ZUV2ZW50cyhldmVudCl7XG4gICAgdmFyIHVybCA9IGV2ZW50LnRhcmdldC5nZXRBdHRyaWJ1dGUoJ2RhdGEtdXJsJyk7XG4gICAgdmFyIGlkID0gZXZlbnQudGFyZ2V0LmdldEF0dHJpYnV0ZSgnZGF0YS11c2VyLWlkJyk7XG5cbiAgICAkLmFqYXgoe1xuICAgICAgICB0eXBlOiBcImRlbGV0ZVwiLFxuICAgICAgICB1cmw6IHVybCxcbiAgICAgICAgc3VjY2VzczpmdW5jdGlvbihkYXRhKXtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGEpO1xuICAgICAgICB9XG4gICAgfSkuZG9uZShmdW5jdGlvbiggZGF0YSApIHtcbiAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI3VzZXItJyArIGlkKS5yZW1vdmUoKTtcbiAgICB9KTtcbn1cblxuZnVuY3Rpb24gcmVzZXRDcmVhdGVVc2VyRm9ybSgpe1xuICAgIHZhciBmb3JtID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI3VzZXItY3JlYXRlJyk7XG4gICAgZm9ybS5xdWVyeVNlbGVjdG9yKCdpbnB1dFtuYW1lPXBhc3N3b3JkXScpLnZhbHVlID0gXCJcIjtcbiAgICBmb3JtLnF1ZXJ5U2VsZWN0b3IoJ2lucHV0W25hbWU9bmFtZV0nKS52YWx1ZSA9IFwiXCI7XG4gICAgZm9ybS5xdWVyeVNlbGVjdG9yQWxsKFwiI3VzZXItY3JlYXRlIGlucHV0XCIpLmZvckVhY2goZnVuY3Rpb24oZWxlbWVudCl7XG4gICAgICAgIGlmIChlbGVtZW50LmdldEF0dHJpYnV0ZSgndHlwZScpID09PSBcImNoZWNrYm94XCIpXG4gICAgICAgICAgICBlbGVtZW50LmNoZWNrZWQgPSBmYWxzZTtcbiAgICB9KTtcbn1cblxuZnVuY3Rpb24gYXBwZW5kTmV3VXNlcih1c2VyKXtcbiAgICB2YXIgbmV3VXNlckZvcm0gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcudXNlci1yb3cnKS5jbG9uZU5vZGUodHJ1ZSk7XG4gICAgdmFyIGlkID0gdXNlci5pZDtcbiAgICBuZXdVc2VyRm9ybS5pZCA9IFwidXNlci1cIiArIGlkO1xuICAgIG5ld1VzZXJGb3JtLnF1ZXJ5U2VsZWN0b3IoJ2lucHV0W25hbWU9bmFtZV0nKS52YWx1ZSA9IHVzZXIubmFtZTtcbiAgICBuZXdVc2VyRm9ybS5xdWVyeVNlbGVjdG9yKCcuYnRuLXVwZGF0ZScpLnNldEF0dHJpYnV0ZSgnZGF0YS11cmwnLCAnaHR0cDovL2xhcmF2ZWwtcHJvamVjdC50ZXN0L3Blcm1pc3Npb24vJyArIGlkKTtcbiAgICBuZXdVc2VyRm9ybS5xdWVyeVNlbGVjdG9yKCcuYnRuLXVwZGF0ZScpLnNldEF0dHJpYnV0ZSgnZGF0YS11c2VyLWlkJywgaWQpO1xuICAgIG5ld1VzZXJGb3JtLnF1ZXJ5U2VsZWN0b3IoJy5idG4tZGVsZXRlJykuc2V0QXR0cmlidXRlKCdkYXRhLXVybCcsICdodHRwOi8vbGFyYXZlbC1wcm9qZWN0LnRlc3QvcGVybWlzc2lvbi8nICsgaWQpO1xuICAgIG5ld1VzZXJGb3JtLnF1ZXJ5U2VsZWN0b3IoJy5idG4tZGVsZXRlJykuc2V0QXR0cmlidXRlKCdkYXRhLXVzZXItaWQnLCBpZCk7XG5cbiAgICB2YXIgaW5wdXRzID0gbmV3VXNlckZvcm0ucXVlcnlTZWxlY3RvckFsbChcImlucHV0XCIpO1xuICAgIGlucHV0cy5mb3JFYWNoKGZ1bmN0aW9uKGVsZW1lbnQpe1xuICAgICAgICBpZiAoZWxlbWVudC5nZXRBdHRyaWJ1dGUoJ3R5cGUnKSA9PT0gXCJjaGVja2JveFwiKVxuICAgICAgICAgICAgZWxlbWVudC5jaGVja2VkID0gZXZhbCgndXNlci5wZXJtaXNzaW9ucy4nICsgZWxlbWVudC5nZXRBdHRyaWJ1dGUoJ25hbWUnKS5yZXBsYWNlKCdwZXJtaXNzaW9uLicsICcnKSk7XG4gICAgfSk7XG5cbiAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjcGVybWlzc2lvbi1jb250ZW50IC5jYXJkJykuaW5zZXJ0QmVmb3JlKG5ld1VzZXJGb3JtLCBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjdXNlci1jcmVhdGUnKSk7XG5cbiAgICB2YXIgZXhpc3RpbmdVc2VyID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmV4aXN0aW5nLXVzZXInKTtcbiAgICBleGlzdGluZ1VzZXJbZXhpc3RpbmdVc2VyLmxlbmd0aCAtIDFdLnF1ZXJ5U2VsZWN0b3IoJy5idG4tdXBkYXRlJykuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBldmVudCA9PiB7dXBkYXRlRXZlbnRzKGV2ZW50KX0pO1xuICAgIGV4aXN0aW5nVXNlcltleGlzdGluZ1VzZXIubGVuZ3RoIC0gMV0ucXVlcnlTZWxlY3RvcignLmJ0bi1kZWxldGUnKS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGV2ZW50ID0+IHtkZWxldGVFdmVudHMoZXZlbnQpfSk7XG59XG4iXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./resources/js/permission.js\n");

/***/ }),

/***/ 3:
/*!******************************************!*\
  !*** multi ./resources/js/permission.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /var/www/laravel-project/resources/js/permission.js */"./resources/js/permission.js");


/***/ })

/******/ });