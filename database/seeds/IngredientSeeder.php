<?php

use Illuminate\Database\Seeder;

class IngredientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ingredientArray = [
            ['oeuf', false],
            ['riz', false],
            ['pate', false],
            ['lardon', true],
            ['crème fraîche', false],
            ['thon', true],
            ['maïs', false],
            ['champignon', false],
            ['farine', false],
            ['sucre', false],
            ['steack', true],
            ['steack-haché', true],
            ['patate', false],
            ['oignon', false],
            ['ail', false],
            ['courgette', false],
            ['tomate', false],
            ['poivron', false],
            ['aubergine', false],
            ['émental', false],
            ['reblochon', false],
            ['raclette', false],
            ['jambon', true],
            ['chèvre', false],
            ['pizza', true],
            ['salade', false],
        ];
        foreach ($ingredientArray as $value){
            DB::table('ingredients')->insert([
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'name' => $value[0],
                'meat' => $value[1],
            ]);
        }
    }
}
