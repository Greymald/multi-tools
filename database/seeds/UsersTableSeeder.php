<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "tom",
            'password' => Hash::make('test'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'permissions' => json_encode(['permission' => true, 'shop' => true, 'shareFiles' => true, 'download' => true])
        ]);

        DB::table('users')->insert([
            'name' => "max",
            'password' => Hash::make('password'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'permissions' => json_encode(['permission' => false, 'shop' => true, 'shareFiles' => true, 'download' => true])
        ]);
    }
}
