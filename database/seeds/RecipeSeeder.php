<?php

use Illuminate\Database\Seeder;

use App\Models\Recipe;
use App\Models\Ingredient;

class RecipeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        $usersIDs = App\Models\User::all()->pluck('id');
        DB::table('recipes')->insert([
            'user_id' => $faker->randomElement($usersIDs),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'name' => 'pates carbo custom',
            'steps' => 'lorem ipsum sit dolor amet'
        ]);

        $ingredients = Ingredient::all();

        $recipe = Recipe::all()->first();
        $recipe->ingredients()->attach(
            $ingredients->where('name', '==', 'pate')->pluck('id')->toArray()
        );
        $recipe->ingredients()->attach(
            $ingredients->where('name', '==', 'champignon')->pluck('id')->toArray()
        );
        $recipe->ingredients()->attach(
            $ingredients->where('name', '==', 'oeuf')->pluck('id')->toArray()
        );
        $recipe->ingredients()->attach(
            $ingredients->where('name', '==', 'lardon')->pluck('id')->toArray()
        );
        $recipe->ingredients()->attach(
            $ingredients->where('name', '==', 'oignon')->pluck('id')->toArray()
        );
        $recipe->ingredients()->attach(
            $ingredients->where('name', '==', 'ail')->pluck('id')->toArray()
        );
        $recipe->ingredients()->attach(
            $ingredients->where('name', '==', 'crème fraîche')->pluck('id')->toArray()
        );
    }
}
